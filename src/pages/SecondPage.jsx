import { useEffect } from "react"
import { useNavigate, useParams, useSearchParams } from "react-router-dom"

const Secondpage = () => {
    const navigate = useNavigate()
    const {param} = useParams()

    // Lấy dữ liệu từ request query url: http://localhost:3000/secondpage/1/2?size=43&type=3
    const [query] = useSearchParams();
    console.log(query.get("type"));
   
    useEffect(() => {
        if(param == 3) {
            navigate("/thirdpage")
        }
    })
    return (
        <div>
            <p>
                Second page
            </p>
            {
                param ?
                <p>Param: {param}</p>
                : <></>
            }

        </div>
    )
}

export default Secondpage