import Homepage from "./pages/HomePage";
import Secondpage from "./pages/SecondPage";
import Firstpage from "./pages/FirstPage";
import ThirdPage from "./pages/ThirdPage";

const routes = [
    {
        path: "/",
        element: <Homepage />
    },
    {
        path: "/firstpage",
        element: <Firstpage />
    },
    {
        path: "/secondpage",
        element: <Secondpage />
    },
    {
        path: "/secondpage/:param",
        element: <Secondpage />
    },
    {
        path: "/thirdpage",
        element: <ThirdPage />
    },
]
export default routes