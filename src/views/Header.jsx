import { Link } from "react-router-dom"

const Header = () => {
    return (
        <div style={{backgroundColor: 'green'}}>
            <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/firstpage">First Page</Link>
            </li>
            <li>
              <Link to="/secondpage">Second Page</Link>
            </li>
            <li>
              <Link to="/thirdpage">Third Page</Link>
            </li>
          </ul>
        </nav>
        </div>
    )
}

export default Header