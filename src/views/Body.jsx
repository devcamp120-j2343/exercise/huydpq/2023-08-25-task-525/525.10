import { Route, Routes } from "react-router-dom"
import routes from "../routes"
import NotFound from "../pages/404"

const Body = () => {
    return (
        <div style={{backgroundColor: "red"}}>
            <Routes>
            {
                routes.map((route, index) => {
                    return <Route path={route.path} element= {route.element} key={index}/>
                })
            }
            <Route path="*" element={<NotFound />}/> 
            </Routes>
        </div>
    )
}

export default Body